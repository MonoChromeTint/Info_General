#!/bin/bash


DEFAULT_ROOT_CONFIG='# OpenSSL root CA configuration file.
# Copy to `/root/ca/openssl.cnf`.

[ ca ]
# `man ca`
default_ca = CA_default

[ CA_default ]
# Directory and file locations.
dir               = /home/user/PKI/CA
certs             = $dir/certs
crl_dir           = $dir/crl
new_certs_dir     = $dir/newcerts
database          = $dir/index.txt
serial            = $dir/serial
RANDFILE          = $dir/private/.rand

# The root key and root certificate.
private_key       = $dir/private/ca.key.pem
certificate       = $dir/certs/ca.cert.pem

# For certificate revocation lists.
crlnumber         = $dir/crlnumber
crl               = $dir/crl/ca.crl.pem
crl_extensions    = crl_ext
default_crl_days  = 30

# SHA-1 is deprecated, so use SHA-2 instead.
default_md        = sha256

name_opt          = ca_default
cert_opt          = ca_default
default_days      = 375
preserve          = no
policy            = policy_strict

[ policy_strict ]
# The root CA should only sign intermediate certificates that match.
# See the POLICY FORMAT section of `man ca`.
countryName             = match
stateOrProvinceName     = match
organizationName        = match
organizationalUnitName  = optional
commonName              = supplied
emailAddress            = optional

[ policy_loose ]
# Allow the intermediate CA to sign a more diverse range of certificates.
# See the POLICY FORMAT section of the `ca` man page.
countryName             = optional
stateOrProvinceName     = optional
localityName            = optional
organizationName        = optional
organizationalUnitName  = optional
commonName              = supplied
emailAddress            = optional

[ req ]
# Options for the `req` tool (`man req`).
default_bits        = 2048
distinguished_name  = req_distinguished_name
string_mask         = utf8only

# SHA-1 is deprecated, so use SHA-2 instead.
default_md          = sha256

# Extension to add when the -x509 option is used.
x509_extensions     = v3_ca

[ req_distinguished_name ]
# See <https://en.wikipedia.org/wiki/Certificate_signing_request>.
countryName                     = Country Name (2 letter code)
stateOrProvinceName             = State or Province Name
localityName                    = Locality Name
0.organizationName              = Organization Name
organizationalUnitName          = Organizational Unit Name
commonName                      = Common Name
emailAddress                    = Email Address

# Optionally, specify some defaults.
countryName_default             = US
stateOrProvinceName_default     = DC
localityName_default            =
0.organizationName_default      = OrgName
organizationalUnitName_default  =
emailAddress_default            =

[ v3_ca ]
# Extensions for a typical CA (`man x509v3_config`).
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid:always,issuer
basicConstraints = critical, CA:true
keyUsage = critical, digitalSignature, cRLSign, keyCertSign

[ v3_intermediate_ca ]
# Extensions for a typical intermediate CA (`man x509v3_config`).
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid:always,issuer
basicConstraints = critical, CA:true, pathlen:10
keyUsage = critical, digitalSignature, cRLSign, keyCertSign

[ usr_cert ]
# Extensions for client certificates (`man x509v3_config`).
basicConstraints = CA:FALSE
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid,issuer
keyUsage = critical, nonRepudiation, digitalSignature, keyEncipherment
extendedKeyUsage = clientAuth, emailProtection

[ server_cert ]
# Extensions for server certificates (`man x509v3_config`).
basicConstraints = CA:FALSE
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid,issuer:always
keyUsage = critical, digitalSignature, keyEncipherment
extendedKeyUsage = serverAuth

[ crl_ext ]
# Extension for CRLs (`man x509v3_config`).
authorityKeyIdentifier=keyid:always

[ ocsp ]
# Extension for OCSP signing certificates (`man ocsp`).
basicConstraints = CA:FALSE
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid,issuer
keyUsage = critical, digitalSignature
extendedKeyUsage = critical, OCSPSigning'




DEFAULT_INTE_CONFIG='# OpenSSL intermediate CA configuration file.
# Copy to `/root/ca/intermediate/openssl.cnf`.

[ ca ]
# `man ca`
default_ca = CA_default

[ CA_default ]
# Directory and file locations.
dir               = /home/user/PKI/CA/intermediate
certs             = $dir/certs
crl_dir           = $dir/crl
new_certs_dir     = $dir/newcerts
database          = $dir/index.txt
serial            = $dir/serial
RANDFILE          = $dir/private/.rand

# The root key and root certificate.
private_key       = $dir/private/intermediate.key.pem
certificate       = $dir/certs/intermediate.cert.pem

# For certificate revocation lists.
crlnumber         = $dir/crlnumber
crl               = $dir/crl/intermediate.crl.pem
crl_extensions    = crl_ext
default_crl_days  = 30

# SHA-1 is deprecated, so use SHA-2 instead.
default_md        = sha256

name_opt          = ca_default
cert_opt          = ca_default
default_days      = 375
preserve          = no
policy            = policy_loose

[ policy_strict ]
# The root CA should only sign intermediate certificates that match.
# See the POLICY FORMAT section of `man ca`.
countryName             = match
stateOrProvinceName     = match
organizationName        = match
organizationalUnitName  = optional
commonName              = supplied
emailAddress            = optional

[ policy_loose ]
# Allow the intermediate CA to sign a more diverse range of certificates.
# See the POLICY FORMAT section of the `ca` man page.
countryName             = optional
stateOrProvinceName     = optional
localityName            = optional
organizationName        = optional
organizationalUnitName  = optional
commonName              = supplied
emailAddress            = optional

[ req ]
# Options for the `req` tool (`man req`).
default_bits        = 2048
distinguished_name  = req_distinguished_name
string_mask         = utf8only

# SHA-1 is deprecated, so use SHA-2 instead.
default_md          = sha256

# Extension to add when the -x509 option is used.
x509_extensions     = v3_ca

[ req_distinguished_name ]
# See <https://en.wikipedia.org/wiki/Certificate_signing_request>.
countryName                     = Country Name (2 letter code)
stateOrProvinceName             = State or Province Name
localityName                    = Locality Name
0.organizationName              = Organization Name
organizationalUnitName          = Organizational Unit Name
commonName                      = Common Name
emailAddress                    = Email Address

# Optionally, specify some defaults.
countryName_default             = US
stateOrProvinceName_default     = DC
localityName_default            =
0.organizationName_default      = OrgName
organizationalUnitName_default  =
emailAddress_default            =

[ v3_ca ]
# Extensions for a typical CA (`man x509v3_config`).
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid:always,issuer
basicConstraints = critical, CA:true
keyUsage = critical, digitalSignature, cRLSign, keyCertSign

[ v3_intermediate_ca ]
# Extensions for a typical intermediate CA (`man x509v3_config`).
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid:always,issuer
basicConstraints = critical, CA:true, pathlen:10
keyUsage = critical, digitalSignature, cRLSign, keyCertSign

[ usr_cert ]
# Extensions for client certificates (`man x509v3_config`).
basicConstraints = CA:FALSE
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid,issuer
keyUsage = critical, nonRepudiation, digitalSignature, keyEncipherment
extendedKeyUsage = clientAuth, emailProtection

[ server_cert ]
# Extensions for server certificates (`man x509v3_config`).
basicConstraints = CA:FALSE
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid,issuer:always
keyUsage = critical, digitalSignature, keyEncipherment
extendedKeyUsage = serverAuth

[ crl_ext ]
# Extension for CRLs (`man x509v3_config`).
authorityKeyIdentifier=keyid:always

[ ocsp ]
# Extension for OCSP signing certificates (`man ocsp`).
basicConstraints = CA:FALSE
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid,issuer
keyUsage = critical, digitalSignature
extendedKeyUsage = critical, OCSPSigning'



DEFAULT_REQ_CONFIG='[ policy_strict ]
# The root CA should only sign intermediate certificates that match.
# See the POLICY FORMAT section of `man ca`.
countryName             = match
stateOrProvinceName     = match
organizationName        = match
organizationalUnitName  = optional
commonName              = supplied
emailAddress            = optional

[ policy_loose ]
# Allow the intermediate CA to sign a more diverse range of certificates.
# See the POLICY FORMAT section of the `ca` man page.
countryName             = optional
stateOrProvinceName     = optional
localityName            = optional
organizationName        = optional
organizationalUnitName  = optional
commonName              = supplied
emailAddress            = optional

[ req ]
# Options for the `req` tool (`man req`).
default_bits        = 2048
distinguished_name  = req_distinguished_name
string_mask         = utf8only

# SHA-1 is deprecated, so use SHA-2 instead.
default_md          = sha256

# Extension to add when the -x509 option is used.
x509_extensions     = v3_ca

[ req_distinguished_name ]
# See <https://en.wikipedia.org/wiki/Certificate_signing_request>.
countryName                     = Country Name (2 letter code)
stateOrProvinceName             = State or Province Name
localityName                    = Locality Name
0.organizationName              = Organization Name
organizationalUnitName          = Organizational Unit Name
commonName                      = Common Name
emailAddress                    = Email Address

# Optionally, specify some defaults.
countryName_default             = US
stateOrProvinceName_default     = DC
localityName_default            =
0.organizationName_default      = OrgName
organizationalUnitName_default  =
emailAddress_default            =

[ v3_ca ]
# Extensions for a typical CA (`man x509v3_config`).
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid:always,issuer
basicConstraints = critical, CA:true
keyUsage = critical, digitalSignature, cRLSign, keyCertSign

[ v3_intermediate_ca ]
# Extensions for a typical intermediate CA (`man x509v3_config`).
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid:always,issuer
basicConstraints = critical, CA:true, pathlen:10
keyUsage = critical, digitalSignature, cRLSign, keyCertSign

[ usr_cert ]
# Extensions for client certificates (`man x509v3_config`).
basicConstraints = CA:FALSE
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid,issuer
keyUsage = critical, nonRepudiation, digitalSignature, keyEncipherment
extendedKeyUsage = clientAuth, emailProtection

[ server_cert ]
# Extensions for server certificates (`man x509v3_config`).
basicConstraints = CA:FALSE
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid,issuer:always
keyUsage = critical, digitalSignature, keyEncipherment
extendedKeyUsage = serverAuth

[ crl_ext ]
# Extension for CRLs (`man x509v3_config`).
authorityKeyIdentifier=keyid:always

[ ocsp ]
# Extension for OCSP signing certificates (`man ocsp`).
basicConstraints = CA:FALSE
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid,issuer
keyUsage = critical, digitalSignature
extendedKeyUsage = critical, OCSPSigning'




THIS_LOC="DEFAULT - THIS_LOC"
certList=("")

listFiles () {
	DIR="$1"
	ARRAYzzz=("")

	echo "Pick upper intermediate: "

	counter=$((counter + 1))
	for i in $(ls -1 $DIR); do
		ARRAYzzz+=("$i")
		echo -e "\t$counter - $i"
		counter=$(($counter + 1))
	done

	read -p "Enter number: " T

	THIS_LOC="${ARRAYzzz[$T]}"

#	echo "$DIR $T - ${ARRAYzzz[$T]}"
}


listCerts () {
	DIR="$1"
	ARRAYzzz=("")

	echo "Pick upper intermediate: "

	counter=$((counter + 1))
	for i in $(ls -1 $DIR); do
		ARRAYzzz+=("$i")
		echo -e "\t$counter - $i"
		counter=$(($counter + 1))
	done

	read -p "Enter number: " T

	THIS_LOC="${ARRAYzzz[$T]}"

#	echo "$DIR $T - ${ARRAYzzz[$T]}"
}



if [[ "$1" == "RESET" ]]; then
	ls ~/PKI/CA/*
#	rm -dR ~/PKI/*
fi

if [[ "$1" == "intCA" ]]; then
	mkdir -p ~/PKI/Configs
	mkdir -p ~/PKI/CA/{certs,crl,newcerts,private,intermediate,sites,leafs}
	mkdir -p ~/PKI/CA/intermediate/{certs,crl,csr,newcerts,private}
	mkdir -p ~/PKI/CA/sites/{certs,crl,csr,newcerts,private}
	mkdir -p ~/PKI/CA/leafs/{certs,crl,csr,newcerts,private}

## Other Files
#	cd ~/PKI/CA/
	chmod 700 ~/PKI/CA/private
	touch ~/PKI/CA/index.txt
	touch ~/PKI/CA/intermediate/index.txt
	touch ~/PKI/CA/sites/index.txt
	touch ~/PKI/CA/leafs/index.txt

#	echo 1000 > ~/PKI/CA/serial
	echo 1000 > ~/PKI/CA/intermediate/crlnumber
	echo 1000 > ~/PKI/CA/intermediate/serial
	echo 1000 > ~/PKI/CA/intermediate/index.txt
	echo 1000 > ~/PKI/CA/sites/serial
	echo 1000 > ~/PKI/CA/sites/crlnumber
	echo 1000 > ~/PKI/CA/leafs/serial
	echo 1000 > ~/PKI/CA/leafs/crlnumber


###########
#	TMP

###########
	echo "$DEFAULT_INTE_CONFIG" > ~/PKI/Configs/intermediate-config.cnf
	echo "$DEFAULT_ROOT_CONFIG" > ~/PKI/Configs/root-config.cnf

# Root
	openssl genrsa -aes256 -out ~/PKI/CA/private/ca.key.pem 4096
	openssl req -config ~/PKI/Configs/root-config.cnf -key ~/PKI/CA/private/ca.key.pem -new -x509 -days 7300 -sha256 -extensions v3_ca -out ~/PKI/CA/certs/ca.cert.pem

	chmod 400 ~/PKI/CA/private/ca.key.pem

# Intermediate
	HereHere=$(realpath ~/PKI/CA/certs/)
	listFiles "$HereHere"

	NewName="$(echo $(echo $THIS_LOC | rev | cut -d'.' -f3- | rev).key.pem)"
	echo "Using --------> $THIS_LOC"

	read -p "Enter Domain Name: " siteName

	echo "$DEFAULT_ROOT_CONFIG" | sed "s/certificate       = \$dir\/certs\/intermediate.cert.pem/certificate       = \$dir\/certs\/$THIS_LOC/g" | sed "s/private_key       = \$dir\/private\/intermediate.key.pem/private_key       = \$dir\/private\/$NewName/g" > ~/PKI/Configs/tmpConfigRoot.cnf
	echo "$DEFAULT_INTE_CONFIG" | sed "s/certificate       = \$dir\/certs\/ca.cert.pem/certificate       = \$dir\/intermediate\/certs\/$THIS_LOC/g" | sed "s/private_key       = \$dir\/private\/ca.key.pem/private_key       = \$dir\/intermediate\/private\/$NewName/g" > ~/PKI/Configs/tmpConfigInt.cnf


	openssl genrsa -aes256 -out ~/PKI/CA/intermediate/private/$siteName.key.pem 4096
	openssl req -config ~/PKI/Configs/tmpConfigInt.cnf -new -sha256 -key ~/PKI/CA/intermediate/private/$siteName.key.pem -out ~/PKI/CA/intermediate/csr/$siteName.csr.pem
	openssl ca -rand_serial -config ~/PKI/Configs/tmpConfigRoot.cnf -extensions v3_intermediate_ca -days 3650 -notext -md sha256 -in ~/PKI/CA/intermediate/csr/$siteName.csr.pem -out ~/PKI/CA/intermediate/certs/$siteName.cert.pem

	chmod 444 ~/PKI/CA/intermediate/certs/$siteName.cert.pem


fi


if [[ "$1" == "v1InterCert" ]]; then

	HereHere=$(realpath ~/PKI/CA/intermediate/certs/)
	listFiles "$HereHere"

	NewName="$(echo $(echo $THIS_LOC | rev | cut -d'.' -f3- | rev).key.pem)"
	echo "Using --------> $THIS_LOC"

	read -p "Enter Intermediate Name: int_" siteName

	siteNameCert="$(echo int_$siteName.cert.pem)"
	siteNameKey="$(echo int_$siteName.key.pem)"

	echo "$siteNameCert = $siteNameKey"

	echo "$DEFAULT_INTE_CONFIG" | sed "s/certificate       = \$dir\/certs\/intermediate.cert.pem/certificate       = \$dir\/certs\/$THIS_LOC/g" | sed "s/private_key       = \$dir\/private\/intermediate.key.pem/private_key       = \$dir\/private\/$NewName/g" > ~/PKI/Configs/tmpConfigInt.cnf
	echo "$DEFAULT_ROOT_CONFIG" | sed "s/certificate       = \$dir\/certs\/ca.cert.pem/certificate       = \$dir\/intermediate\/certs\/$THIS_LOC/g" | sed "s/private_key       = \$dir\/private\/ca.key.pem/private_key       = \$dir\/intermediate\/private\/$NewName/g" > ~/PKI/Configs/tmpConfigRoot.cnf



	openssl genrsa -aes256 -out ~/PKI/CA/intermediate/private/int_$siteName.key.pem 4096
	openssl req -config ~/PKI/Configs/tmpConfigInt.cnf -new -sha256 -key ~/PKI/CA/intermediate/private/int_$siteName.key.pem -out ~/PKI/CA/intermediate/csr/int_$siteName.csr.pem
	openssl ca -rand_serial -config ~/PKI/Configs/tmpConfigRoot.cnf -extensions v3_intermediate_ca -days 3650 -notext -md sha256 -in ~/PKI/CA/intermediate/csr/int_$siteName.csr.pem -out ~/PKI/CA/intermediate/certs/int_$siteName.cert.pem

fi

if [[ "$1" == "vNInterCert" ]]; then
	HereHere=$(realpath ~/PKI/CA/intermediate/certs/)
	listFiles "$HereHere"

	NewName="$(echo $(echo $THIS_LOC | rev | cut -d'.' -f3- | rev).key.pem)"
	echo "Using --------> $THIS_LOC"

	read -p "Enter Sub Intermediate Name: int_" siteName

	siteNameCert="$(echo int_$siteName.cert.pem)"
	siteNameKey="$(echo int_$siteName.key.pem)"

	echo "$siteNameCert = $siteNameKey"

	echo "$DEFAULT_INTE_CONFIG" | sed "s/certificate       = \$dir\/certs\/intermediate.cert.pem/certificate       = \$dir\/certs\/$THIS_LOC/g" | sed "s/private_key       = \$dir\/private\/intermediate.key.pem/private_key       = \$dir\/private\/$NewName/g" > ~/PKI/Configs/tmpConfigInt.cnf
#	echo "$DEFAULT_ROOT_CONFIG" | sed "s/certificate       = \$dir\/certs\/ca.cert.pem/certificate       = \$dir\/certs\/$siteNameCert/g" | sed "s/private_key       = \$dir\/private\/ca.key.pem/private_key       = \$dir\/private\/$siteNameKey/g" | sed "s/dir               = \/home\/user\/PKI\/CA/dir               = \/home\/user\/PKI\/CA\/intermediate/g" > ~/PKI/Configs/tmpConfigRoot.cnf
	echo "$DEFAULT_ROOT_CONFIG" | sed "s/certificate       = \$dir\/certs\/ca.cert.pem/certificate       = \$dir\/intermediate\/certs\/$THIS_LOC/g" | sed "s/private_key       = \$dir\/private\/ca.key.pem/private_key       = \$dir\/intermediate\/private\/$NewName/g" > ~/PKI/Configs/tmpConfigRoot.cnf

	openssl genrsa -aes256 -out ~/PKI/CA/intermediate/private/int_$siteName.key.pem 4096


	openssl req -config ~/PKI/Configs/tmpConfigInt.cnf -new -sha256 -key ~/PKI/CA/intermediate/private/int_$siteName.key.pem -out ~/PKI/CA/intermediate/csr/int_$siteName.csr.pem
	openssl ca -rand_serial -config ~/PKI/Configs/tmpConfigRoot.cnf -extensions v3_intermediate_ca -days 3650 -notext -md sha256 -in ~/PKI/CA/intermediate/csr/int_$siteName.csr.pem -out ~/PKI/CA/intermediate/certs/int_$siteName.cert.pem

fi

if [[ "$1" == "siteCert" ]]; then

	HereHere=$(realpath ~/PKI/CA/intermediate/certs/)
	listFiles "$HereHere"

	NewName="$(echo $(echo $THIS_LOC | rev | cut -d'.' -f3- | rev).key.pem)"
	echo "Using --------> $THIS_LOC"

	read -p "Enter Site Name: site_" siteName

	echo "SAN Example: 'subjectAltName = DNS:example.com, DNS:www.example.com, IP:192.168.1.2, IP:192.168.1.3'"
	read -p "Enter Site subjectAltName = " sAN

	sAN=$(echo "subjectAltName = $sAN")

	siteNameCert="$(echo site_$siteName.cert.pem)"
	siteNameKey="$(echo site_$siteName.key.pem)"

	echo "$siteNameCert = $siteNameKey"

	echo "$DEFAULT_INTE_CONFIG" | sed "s/certificate       = \$dir\/certs\/intermediate.cert.pem/certificate       = \$dir\/certs\/$THIS_LOC/g" | sed "s/private_key       = \$dir\/private\/intermediate.key.pem/private_key       = \$dir\/private\/$NewName/g" | sed "s/extendedKeyUsage = serverAuth/extendedKeyUsage = serverAuth\n$sAN/g"  > ~/PKI/Configs/tmpConfigInt.cnf
	echo "$DEFAULT_ROOT_CONFIG" | sed "s/certificate       = \$dir\/certs\/ca.cert.pem/certificate       = \$dir\/intermediate\/certs\/$THIS_LOC/g" | sed "s/private_key       = \$dir\/private\/ca.key.pem/private_key       = \$dir\/intermediate\/private\/$NewName/g" | sed "s/extendedKeyUsage = serverAuth/extendedKeyUsage = serverAuth\n$sAN/g" > ~/PKI/Configs/tmpConfigRoot.cnf

	openssl genrsa -out ~/PKI/CA/sites/private/site_$siteName.key.pem 4096

	openssl req -config ~/PKI/Configs/tmpConfigInt.cnf -new -sha256 -key ~/PKI/CA/sites/private/site_$siteName.key.pem -out ~/PKI/CA/sites/csr/site_$siteName.csr.pem
	openssl ca -rand_serial -config ~/PKI/Configs/tmpConfigRoot.cnf -extensions server_cert -days 365 -notext -md sha256 -in ~/PKI/CA/sites/csr/site_$siteName.csr.pem -out ~/PKI/CA/sites/certs/site_$siteName.cert.pem


fi

if [[ "$1" == "leafCert" ]]; then
	HereHere=$(realpath ~/PKI/CA/intermediate/certs/)
	listFiles "$HereHere"

	NewName="$(echo $(echo $THIS_LOC | rev | cut -d'.' -f3- | rev).key.pem)"
	echo "Using --------> $THIS_LOC"

	read -p "Enter Leaf Name: leaf_" siteName

	siteNameCert="$(echo leaf_$siteName.cert.pem)"
	siteNameKey="$(echo leaf_$siteName.key.pem)"

	echo "$siteNameCert = $siteNameKey"

	echo "$DEFAULT_INTE_CONFIG" | sed "s/certificate       = \$dir\/certs\/intermediate.cert.pem/certificate       = \$dir\/certs\/$THIS_LOC/g" | sed "s/private_key       = \$dir\/private\/intermediate.key.pem/private_key       = \$dir\/private\/$NewName/g" > ~/PKI/Configs/tmpConfigInt.cnf
	echo "$DEFAULT_ROOT_CONFIG" | sed "s/certificate       = \$dir\/certs\/ca.cert.pem/certificate       = \$dir\/intermediate\/certs\/$THIS_LOC/g" | sed "s/private_key       = \$dir\/private\/ca.key.pem/private_key       = \$dir\/intermediate\/private\/$NewName/g" > ~/PKI/Configs/tmpConfigRoot.cnf

	openssl genrsa -out ~/PKI/CA/leafs/private/leaf_$siteName.key.pem 4096

	openssl req -config ~/PKI/Configs/tmpConfigInt.cnf -new -sha256 -key ~/PKI/CA/leafs/private/leaf_$siteName.key.pem -out ~/PKI/CA/leafs/csr/leaf_$siteName.csr.pem
	openssl ca -rand_serial -config ~/PKI/Configs/tmpConfigRoot.cnf -extensions usr_cert -days 730 -notext -md sha256 -in ~/PKI/CA/leafs/csr/leaf_$siteName.csr.pem -out ~/PKI/CA/leafs/certs/leaf_$siteName.cert.pem

fi



if [[ "$1" == "reqSite" ]]; then
	tmpFile=$(mktemp)


	echo "Domain Name example: nginx.services.local.domain.lan"
	read -p "Domain Name: " siteName

	siteName=$(echo "$siteName" | cut -d'.' -f1)

	echo "SAN Example: 'subjectAltName = DNS:example.com, DNS:www.example.com, IP:192.168.1.2, IP:192.168.1.3'"
	read -p "Enter Site subjectAltName = " sAN

	sAN=$(echo "subjectAltName = $sAN")


	echo "$DEFAULT_REQ_CONFIG" | sed "s/extendedKeyUsage = serverAuth/extendedKeyUsage = serverAuth\n$sAN/g" > $tmpFile

	openssl genrsa -out ./$siteName.key 2048
	openssl req -config $tmpFile -new -sha256 -key ./$siteName.key -out ./$siteName.csr

	rm $tmpFile
fi

if [[ "$1" == "signSite" ]]; then
	HereHere=$(realpath ~/PKI/CA/intermediate/certs/)
	listFiles "$HereHere"

	NewName="$(echo $(echo $THIS_LOC | rev | cut -d'.' -f3- | rev).key.pem)"
	echo "Using --------> $THIS_LOC"

	read -p "Enter CSR to Sign: " siteName

	echo "SAN Example: 'subjectAltName = DNS:example.com, DNS:www.example.com, IP:192.168.1.2, IP:192.168.1.3'"
	read -p "Enter Site subjectAltName = " sAN

	sAN=$(echo "subjectAltName = $sAN")

	echo "$DEFAULT_ROOT_CONFIG" | sed "s/certificate       = \$dir\/certs\/ca.cert.pem/certificate       = \$dir\/intermediate\/certs\/$THIS_LOC/g" | sed "s/private_key       = \$dir\/private\/ca.key.pem/private_key       = \$dir\/intermediate\/private\/$NewName/g" | sed "s/extendedKeyUsage = serverAuth/extendedKeyUsage = serverAuth\n$sAN/g" > ~/PKI/Configs/tmpConfigRoot.cnf

	newSiteName=$(echo $(echo "$siteName" | cut -d'.' -f1).$(echo $THIS_LOC | cut -d'_' -f2-) | rev | cut -d'.' -f3- | rev)

	cp $siteName ~/PKI/CA/sites/csr/site_$newSiteName.csr.pem
	openssl ca -rand_serial -config ~/PKI/Configs/tmpConfigRoot.cnf -extensions server_cert -days 365 -notext -md sha256 -in ~/PKI/CA/sites/csr/site_$newSiteName.csr.pem -out ~/PKI/CA/sites/certs/site_$newSiteName.cert.pem

fi

if [[ "$1" == "reqUser" ]]; then
	echo -e "x509 subject name Example:   C=US, ST=DC, O=OrgName, CN=ESS-AAA.users.local.domain.lan\n\n"

	gpgsm --gen-key -o cert.csr


	echo -e "\n\n"
	read -p "NOTE THE ID THEN SEND TO CA TO GET IT SIGNED"
	echo -e "\n\n"

	gpgsm --import cert.crt

	echo -e "\n\n"
	gpgsm -K

	read -p "Enter cert ID: " certID

	gpgsm --export-secret-key-raw -a -o cert.key $certID

	openssl pkcs12 -export -in cert.crt -inkey cert.key -out cert.p12
fi

if [[ "$1" == "signUser" ]]; then

	HereHere=$(realpath ~/PKI/CA/intermediate/certs/)
	listFiles "$HereHere"

	NewName="$(echo $(echo $THIS_LOC | rev | cut -d'.' -f3- | rev).key.pem)"
	echo "Using --------> $THIS_LOC"

	read -p "Enter CSR to Sign: " siteName

	echo "$DEFAULT_ROOT_CONFIG" | sed "s/certificate       = \$dir\/certs\/ca.cert.pem/certificate       = \$dir\/intermediate\/certs\/$THIS_LOC/g" | sed "s/private_key       = \$dir\/private\/ca.key.pem/private_key       = \$dir\/intermediate\/private\/$NewName/g" | sed 's/policy            = policy_strict/policy            = policy_loose/g' > ~/PKI/Configs/tmpConfigRoot.cnf

	newSiteName=$(echo $(echo "$siteName" | cut -d'.' -f1).$(echo $THIS_LOC | cut -d'_' -f2-) | rev | cut -d'.' -f3- | rev)

	cp $siteName ~/PKI/CA/leafs/csr/leaf_$newSiteName.csr.pem
	openssl ca -rand_serial -config ~/PKI/Configs/tmpConfigRoot.cnf -extensions usr_cert -days 730 -notext -md sha256 -in ~/PKI/CA/leafs/csr/leaf_$newSiteName.csr.pem -out ~/PKI/CA/leafs/certs/leaf_$newSiteName.cert.pem
fi



if [[ "$1" == "bundleCA" ]]; then
	cat ~/PKI/CA/certs/* ~/PKI/CA/intermediate/certs/* > ~/PKI/Bundle.CA
fi

if [[ "$1" == "bundleSite" ]]; then
	HereHere=$(realpath ~/PKI/CA/sites/certs/)
	listFiles "$HereHere"

	NewName="$(echo $(echo $THIS_LOC | rev | cut -d'.' -f3- | rev).key.pem)"
	echo "Using --------> $THIS_LOC"

	Name=$(echo $THIS_LOC | cut -d'.' -f2-)

	res="${Name//[^.]}"
	count="$((${#res}-3))"
	count="$(($count+1))"

	echo "$Name - $count"

	certList+=($(ls ~/PKI/CA/sites/certs/$THIS_LOC))

	for i in $(seq 2 $count); do
		if [[ "$i" == "2" ]]; then
			b="$i"
			b=$((b+1))
			certList+=($(ls ~/PKI/CA/intermediate/certs/int_users.$(echo $THIS_LOC | cut -d'.' -f$b-)))
			certList+=($(ls ~/PKI/CA/intermediate/certs/int_devices.$(echo $THIS_LOC | cut -d'.' -f$b-)))
		fi
		e=$(ls ~/PKI/CA/intermediate/certs/int_$(echo $THIS_LOC | cut -d'.' -f$i-))
		certList+=("$e")
	done

	count="$(($count+1))"
	e=$(ls ~/PKI/CA/intermediate/certs/$(echo $THIS_LOC | cut -d'.' -f$count-))
	certList+=("$e")

	certList+=($(echo ~/PKI/CA/certs/$(ls -1 ~/PKI/CA/certs/ | head -1)))

	echo "" > ~/PKI/$(echo $THIS_LOC | cut -d'.' -f1)
	for i in "${certList[@]}"; do
		if [[ -f "$i" ]]; then
			cat "$i" >> ~/PKI/$(echo $THIS_LOC | cut -d'.' -f1)
		fi
	done

fi

if [[ "$1" == "bundleLeaf" ]]; then
	HereHere=$(realpath ~/PKI/CA/leafs/certs/)
	listFiles "$HereHere"

	NewName="$(echo $(echo $THIS_LOC | rev | cut -d'.' -f3- | rev).key.pem)"
	echo "Using --------> $THIS_LOC"

	Name=$(echo $THIS_LOC | cut -d'.' -f2-)

	res="${Name//[^.]}"
	count="$((${#res}-3))"
	count="$(($count+1))"

	echo "$Name - $count"

	certList+=($(ls ~/PKI/CA/leafs/certs/$THIS_LOC))

	for i in $(seq 2 $count); do
		if [[ "$i" == "2" ]]; then
			b="$i"
			b=$((b+1))
			certList+=($(ls ~/PKI/CA/intermediate/certs/int_users.$(echo $THIS_LOC | cut -d'.' -f$b-)))
		fi
		e=$(ls ~/PKI/CA/intermediate/certs/int_$(echo $THIS_LOC | cut -d'.' -f$i-))
		certList+=("$e")
	done

	count="$(($count+1))"
	e=$(ls ~/PKI/CA/intermediate/certs/$(echo $THIS_LOC | cut -d'.' -f$count-))
	certList+=("$e")

	certList+=($(echo ~/PKI/CA/certs/$(ls -1 ~/PKI/CA/certs/ | head -1)))

	cat ~/PKI/CA/leafs/certs/$THIS_LOC > ~/PKI/$(echo $THIS_LOC | cut -d'.' -f1).pem

	echo "" > ~/PKI/$(echo $THIS_LOC | cut -d'.' -f1).bundle.pem
	for i in "${certList[@]}"; do
		if [[ -f "$i" ]]; then
			cat "$i" >> ~/PKI/$(echo $THIS_LOC | cut -d'.' -f1).bundle.pem
		fi
	done







fi










