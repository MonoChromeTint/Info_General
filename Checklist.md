# General Checklist

## Services:
  - VPN: 
    - [x] Wireguard
    - [ ] Openvpn
    - [ ] DN42(?)

  - Security:
    - [x] ~~Authelia~~
    - [ ] ~~Guacamole(?)~~
    - [ ] ~~Hashicorp Vault~~
    - [x] Ansible-vault
    - [x] Authentik
    - [ ] endlessh
    - [ ] Crowdsource
    - [ ] IDS/IPS

  - Load Balancing:
    - [ ] ~~Traefik~~
    - [x] Nginx
    - [x] Nginx Proxy Manager

  - Management:
    - [x] ~~K3s~~
      - []  ~~Nodes +5~~
    - [x] Proxmox
      - [x] HA
    - [x] Docker_Swarm
      - []  Nodes +5


  - DNS:
    - [ ] ~~Pihole~~
    - [ ] Bind
    - [ ] Unbound
    - [x] Techntium

  - Uptime:
    - [x] ~~Heimdall~~
    - [x] benphelps/homepage
    - [x] Uptime-Kuma

  - Logs:
    - [x] Grafana
      - [x] OpenWRT
      - [x] K3s Nodes
      - [x] Debian VMs
      - [x] Servers
    - [x] Promethus
    - [ ] ~~Loki~~

  - Backup:
    - [x] Syncthing
    - [ ] Borg
    - [ ] Rsync

  - Code:
    - [x] ~~Gitlab~~
    - [x] gitea




## Unrelated:
  - [ ] MiniO
  - [ ] Blog
  - [ ] Calibre
  - [ ] Wallabag
  - [ ] Matomo
  - [ ] Vaultwarden
  - [ ] REMNux
